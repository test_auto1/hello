import allure
import pytest


def setup():
    print('前置清楚')


def teardown():
    print('后置恢复')


@allure.title('输出哈哈哈哈哈')
def test__01():
    print('哈哈哈哈哈哈哈')
    assert True


@allure.title('输出嘿嘿嘿失败')
def test__02():
    print('哈哈哈哈哈哈哈')
    assert 1 == 2


@allure.title('断言通过')
def test__03():
    print('集成到Jenkins')
    assert True


if __name__ == '__main__':
    pytest.main(['-vs'])
